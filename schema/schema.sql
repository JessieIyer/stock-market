create database stocks;
-- int id; String stockTicker; double price; int volume; String buyOrSell;int statusCode;

use stocks;
create table stockinfo(
	id  int auto_increment Primary Key,
    stockTicker varchar(40),
    price double,
    volume int,
    buyOrSell varchar(40),
    statusCode int
    );
    
-- AMZN, AAPL, C, NFLX
insert into stockinfo values (1,"APPLE",148.60,100,"BUY",0);
insert into stockinfo values (2,"ADOBE",658.52,200, "SELL",1);
insert into stockinfo values (3,"CITI",72.99,300, "BUY",0);
insert into stockinfo values (4,"VISA",232.69,400, "BUY",2);
insert into stockinfo values (5,"GOOGLE",2880.08,400, "SELL",1);
insert into stockinfo values (6,"NETFLIX",558.92,400, "BUY",2);
insert into stockinfo values (7,"ATLASSIAN",358.34,400, "BUY",2);
insert into stockinfo values (8,"MICROSOFT",299.72,400, "BUY",2);
insert into stockinfo values (9,"TESLA",711.92,400, "BUY",2);


delete from stockinfo where id in (201,202,203,204);
update stockinfo set volume =0;
select * from stockinfo;

